﻿#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/typeutilities.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/multitypeblockmatrix.hh>
#include <dune/istl/multitypeblockvector.hh>

#include <dune/matrix-vector/algorithm.hh>
#include <dune/matrix-vector/resize.hh>

/// custom multitype types to allow for BlockVector nesting
template <class... Args>
struct CustomMultiTypeBlockVector : public Dune::MultiTypeBlockVector<Args...> {
  constexpr static int blocklevel = 1; // fake value
};
template <class... Args>
struct CustomMultiTypeBlockMatrix : public Dune::MultiTypeBlockMatrix<Args...> {
  constexpr static int blocklevel = 1; // fake value
};
// inject matrix traits for CustomMultiTypeBlockMatrix
namespace Dune { namespace MatrixVector { namespace Traits {
  template <class... Args>
  struct MatrixTraits<CustomMultiTypeBlockMatrix<Args...>> {
    constexpr static bool isMatrix = true;
  };
}}}
// inject vector identification trait for CustomMultiTypeBlockVector
namespace Dune { namespace MatrixVector { namespace Traits {
  template <class... Args>
  struct VectorTraits<CustomMultiTypeBlockVector<Args...>> {
    constexpr static bool isVector = true;
  };
}}}


// Specialize FieldTraits, because this is now a hard requirement
namespace Dune {

template <class... Args>
struct FieldTraits<CustomMultiTypeBlockVector<Args...>>
  : public FieldTraits<Dune::MultiTypeBlockVector<Args...>>
{};

template <class... Args>
struct FieldTraits<CustomMultiTypeBlockMatrix<Args...>>
  : public FieldTraits<Dune::MultiTypeBlockMatrix<Args...>>
{};

} // namespace Dune

// Implement tuple protocol for custom matrix
namespace std
{
  template <size_t i, typename... Args>
  struct tuple_element<i,CustomMultiTypeBlockMatrix<Args...> >
  {
    using type = typename std::tuple_element<i, std::tuple<Args...> >::type;
  };

  /** \brief Make std::tuple_size work for MultiTypeBlockMatrix
   *
   * It derives from std::tuple after all.
   */
  template <typename... Args>
  struct tuple_size<CustomMultiTypeBlockMatrix<Args...> >
    : std::integral_constant<std::size_t, sizeof...(Args)>
  {};
}



class ResizeTestSuite {
  /// typedefs
  using BS = std::bitset<2>;
  using FV = Dune::FieldVector<double, 3>;
  using BV = Dune::BlockVector<FV>;
  using MV = CustomMultiTypeBlockVector<BV, BV, FV>;
  using FM = Dune::FieldMatrix<double, 3, 10>;
  using BM = Dune::BCRSMatrix<FM>;
  using MM = CustomMultiTypeBlockMatrix<
               CustomMultiTypeBlockMatrix<BM>,
               CustomMultiTypeBlockMatrix<BM>,
               CustomMultiTypeBlockMatrix<FM>>;
  /// instantiation helper
  auto createBM(size_t n) {
    BM bm;
    Dune::MatrixIndexSet indices(n, n+10);
    indices.exportIdx(bm);
    return bm;
  }
  auto createMM(size_t n1, size_t n2) {
    using namespace Dune::Indices;
    MM mm;
    mm[_0][_0] = createBM(n1);
    mm[_1][_0] = createBM(n2);
    return mm;
  }

bool checkResize() {
  using namespace Dune::MatrixVector;
  std::cout << "Testing resize... ";
  // test failure for invalid static resize
  try {
    FV fv;
    resize(fv, Dune::FieldVector<int, 5>());
    return false;
  } catch (Dune::Exception&) {
    // TODO make sure the right exception is thrown
  }

  auto hasSize = [](auto&& size, auto&& expectedSize) {
    if(size != expectedSize)
      DUNE_THROW(Dune::Exception, "Resize created unexpected size.");
  };

  try {
    // test vector types
    BS bs, bs2;
    resize(bs, bs2);
    FV fv, fv2;
    resize(fv, fv2);
    BV bv, bv2(5);
    resize(bv, bv2);
    hasSize(bv.size(), (uint) 5);
    MV mv, mv2;
    using namespace Dune::Indices;
    mv2[_0].resize(5);
    resize(mv, mv2);
    hasSize(mv[_0].size(), (uint) 5);
    hasSize(mv[_1].size(), (uint) 0);
    hasSize(mv[_2].size(), (uint) 3);
    // test "natural" matrix types
    FM fm;
    resize(fv, fm);
    BM bm = createBM(4);
    resize(bv, bm);
    hasSize(bv.size(), (uint) 4);
    MM mm = createMM(0, 6);
    resize(mv, mm);
    hasSize(mv[_0].size(), (uint) 0);
    hasSize(mv[_1].size(), (uint) 6);
    hasSize(mv[_2].size(), (uint) 3);
    // test "unnatural" matrix types
    // TODO

  } catch (Dune::Exception& e) {
    std::cout << "FAILURE." << std::endl;
    std::cout << e.what() << std::endl;
    return false;
  }

  std::cout << "done." << std::endl;
  return true;
}

public:

bool check() {
  std::cout << "Resize test starting.." << std::endl;
  bool passed = true;

  passed = passed and checkResize();
  // TODO test ignore nodes type

  if(passed)
    std::cout << "Resize test succeeded.\n" << std::endl;
  return passed;
}

};

int main(int argc, char* argv[]) {
  Dune::MPIHelper::instance(argc, argv);

  bool passed = ResizeTestSuite().check();


  if(not passed)
    return 1;

  return 0;
}
