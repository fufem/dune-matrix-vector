﻿#ifndef DUNE_MATRIX_VECTOR_GENERICVECTORTOOLS_HH
#define DUNE_MATRIX_VECTOR_GENERICVECTORTOOLS_HH

/** \file
 *  \brief Various tools for working with istl vectors of arbitrary nesting depth
 */

#include <bitset>
#include <iostream>
#include <vector>

#include <dune/common/fvector.hh>
#include <dune/common/hybridutilities.hh>

#include <dune/matrix-vector/algorithm.hh>
#include <dune/matrix-vector/traits/utilities.hh>

//! \brief Various tools for working with istl vectors of arbitrary nesting depth
namespace Dune {
namespace MatrixVector {
namespace Generic { // TODO change namespace name

// forward declaration for helper struct
namespace Impl {
template <class Vector, typename Enable = void>
struct ScalarSwitch;
} // end namespace Impl

//! Write vector to given stream
template <class Vector>
void writeBinary(std::ostream& s, const Vector& v) {
  Impl::ScalarSwitch<Vector>::writeBinary(s, v);
}

//! Read vector from given stream
template <class Vector>
void readBinary(std::istream& s, Vector& v) {
  Impl::ScalarSwitch<Vector>::readBinary(s, v);
}

//! Truncate vector by given bit set
template <class Vector, class BitVector>
void truncate(Vector& v, const BitVector& tr) {
  Impl::ScalarSwitch<Vector>::truncate(v, tr);
}

namespace Impl {

//! recursion helper for scalars nested in iterables (vectors)
template <class Vector>
struct ScalarSwitch<Vector, DisableNumber<Vector>> {

  static void writeBinary(std::ostream& s, const Vector& v) {
    Hybrid::forEach(v, [&s](auto&& vi) { Generic::writeBinary(s, vi); });
  }

  static void readBinary(std::istream& s, Vector& v) {
    Hybrid::forEach(v, [&s](auto&& vi) { Generic::readBinary(s, vi); });
  }

  template <class BitVector>
  static void truncate(Vector& v, const BitVector& tr) {
    sparseRangeFor(
        v, [&tr](auto&& vi, auto&& i) { Generic::truncate(vi, tr[i]); });
  }

};

//! recursion anchors for scalars
template <class Scalar>
struct ScalarSwitch<Scalar, EnableNumber<Scalar>> {

  static void writeBinary(std::ostream& s, const Scalar& v) {
    s.write(reinterpret_cast<const char*>(&v), sizeof(Scalar));
  }

  static void readBinary(std::istream& s, Scalar& v) {
    s.read(reinterpret_cast<char*>(&v), sizeof(Scalar));
  }

  template <class BitVector>
  static void truncate(Scalar& v, const BitVector& tr) {
    if (tr)
      v = 0;
  }

};

} // end namespace Impl

} // end namespace Generic
} // end namespace MatrixVector
} // end namespace Dune

#endif // DUNE_MATRIX_VECTOR_GENERICVECTORTOOLS_HH
