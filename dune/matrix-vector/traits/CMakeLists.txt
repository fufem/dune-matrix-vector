#install headers
install(FILES
  fieldtraits.hh
  isquadratic.hh
  matrixtraits.hh
  scalartraits.hh
  utilities.hh
  vectortraits.hh
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/matrix-vector/traits)
