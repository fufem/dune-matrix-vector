﻿#ifndef DUNE_MATRIX_VECTOR_TRAITS_ISQUADRATIC_HH
#define DUNE_MATRIX_VECTOR_TRAITS_ISQUADRATIC_HH

#include <dune/matrix-vector/traits/utilities.hh>

/**
 * \file
 * \brief This may yield incorrect results for types whose ScalarTraits or
 * MatrixTraits specializations are incomplete. Will report the dimension > 0
 * in case the type is detected to be symmetric.
 */

namespace Dune {
namespace MatrixVector {
namespace Traits {

template <class, class Enable = void>
struct IsQuadratic {
  constexpr static int dimension = 0;
};

template <class T>
struct IsQuadratic<T, EnableScalar<T>> {
  constexpr static int dimension = 1;
};

template <class T>
using EnableNonScalarQuadraticMatrix =
    std::enable_if_t<isMatrix<T>() and (MatrixTraits<T>::rows > 1) and
                     MatrixTraits<T>::rows == MatrixTraits<T>::cols>;

template <class T>
struct IsQuadratic<T, EnableNonScalarQuadraticMatrix<T>> {
  constexpr static int dimension = MatrixTraits<T>::rows;
};

} // end namespace Traits
} // end namespace MatrixVector
} // end namespace Dune

#endif // DUNE_MATRIX_VECTOR_TRAITS_ISQUADRATIC_HH
