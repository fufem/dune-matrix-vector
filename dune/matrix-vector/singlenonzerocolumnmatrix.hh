// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_SINGLE_NONZERO_COLUMN_MATRIX_HH
#define DUNE_FUFEM_SINGLE_NONZERO_COLUMN_MATRIX_HH

#include <dune/common/fmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dune/matrix-vector/traits/matrixtraits.hh>

#include <dune/fufem/indexedsliceiterator.hh>

// TODO fix namespcae (also for the MatrixTraits injection down below)

/**
 * \brief A static matrix that has only a single nonzero column
 *
 * The number and values of this column are dynamic.
 * \tparam Storage values of nonzero column are copied and hold in an array. Pass a const reference type to avoid this, e.g. FieldMatrix<K, 1, ROWS>.
 */
template<class K, int ROWS, int COLS, class Storage = Dune::array<K, ROWS> >
class SingleNonZeroColumnMatrix
{
    class RowProxy
    {
        typedef typename std::size_t size_type;
    public:
        typedef IndexedSliceIterator<const K*, const K> ConstIterator;
        typedef ConstIterator const_iterator;

        RowProxy(const K& value, size_type nzCol) :
            value_(value),
            nzCol_(nzCol)
        {}

        ConstIterator begin() const
        {
            return ConstIterator(&value_, nzCol_, 0, 1);
        }

        ConstIterator end() const
        {
            return ConstIterator(&value_, nzCol_+1, 0, 1);
        }

    protected:
        const K& value_;
        size_type nzCol_;
    };

public:
    constexpr static int rows = ROWS;
    constexpr static int cols = COLS;

    typedef RowProxy row_type;
    typedef row_type const_row_reference;
    typedef typename std::size_t size_type;
    typedef typename RowProxy::ConstIterator ConstColIterator;


    /**
     * \brief Create from single column matrix and column index
     */
    template<class NonZeroColumn>
    SingleNonZeroColumnMatrix(const NonZeroColumn& c, size_type columnIndex) :
        nonZeroColumn_(c),
        columnIndex_(columnIndex)
    {}

    size_type N() const
    {
        return ROWS;
    }

    size_type M() const
    {
        return COLS;
    }

    template<class X , class Y >
    void umv(const X& x, Y& y) const
    {
        for(size_type i=0; i<N(); ++i)
            y[i] += nonZeroColumn_[i] * x[columnIndex_];
    }

    template<class X , class Y >
    void umtv(const X& x, Y& y) const
    {
        for(size_type i=0; i<N(); ++i)
            y[columnIndex_] += nonZeroColumn_[i] * x[i];
    }

    template<class X , class Y >
    void mtv(const X& x, Y& y) const
    {
        y = 0.0;
        umtv(x, y);
    }

    const_row_reference operator[] (size_type rowIndex) const
    {
        return const_row_reference(static_cast<const K&>(nonZeroColumn_[rowIndex]), columnIndex_);
    }

    size_type nonZeroColumnIndex() const
    {
        return columnIndex_;
    }

protected:
    Storage nonZeroColumn_;

    const size_type columnIndex_;
};


namespace Dune::MatrixVector::Traits
{
    template<class K, int ROWS, int COLS>
    struct MatrixTraits<SingleNonZeroColumnMatrix<K, ROWS, COLS> >
    {
        constexpr static bool isMatrix = true;
        constexpr static bool isDense = false;
        constexpr static bool sizeIsStatic = true;  // TODO only one of these should be used
        constexpr static bool hasStaticSize = true; // TODO only one of these should be used
        constexpr static int rows = ROWS;
        constexpr static int cols = COLS;
        /*
        typedef typename SingleNonZeroColumnMatrix<K, ROWS, COLS>::ConstColIterator ConstColIterator;

        static ConstColIterator rowBegin(const typename SingleNonZeroColumnMatrix<K, ROWS, COLS>::row_type& m, int row)
        {
            int nzCol = m.nonZeroColumnIndex();
            return ConstColIterator(m[row][nzCol], nzCol);
        }

        static ConstColIterator rowEnd(const typename SingleNonZeroColumnMatrix<K, ROWS, COLS>::row_type& m, int row)
        {
            int nzCol = m.nonZeroColumnIndex();
            return ConstColIterator(m[row][nzCol], nzCol+1);
        }
        */
    };
};

#endif
