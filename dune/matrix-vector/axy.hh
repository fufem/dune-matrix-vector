#ifndef DUNE_MATRIX_VECTOR_AXY_HH
#define DUNE_MATRIX_VECTOR_AXY_HH

#include <cassert>

#include <dune/matrix-vector/algorithm.hh>
#include <dune/matrix-vector/axpy.hh>
#include <dune/matrix-vector/traits/utilities.hh>

namespace Dune {
namespace MatrixVector {

  /** \brief Internal helper class for Matrix operations
   *
   */
  template <class T, typename Enable = void>
  struct OperatorHelper;

  template <class OperatorType>
  struct OperatorHelper<OperatorType, DisableMatrix<OperatorType>> {
    template <class VectorType, class VectorType2>
    static typename VectorType::field_type Axy(const OperatorType &A,
                                               const VectorType &x,
                                               const VectorType2 &y) {
      VectorType2 tmp = y;
      tmp = 0;
      addProduct(tmp, A, x);
      return tmp * y;
    }

    template <class VectorType, class VectorType2>
    static typename VectorType::field_type bmAxy(const OperatorType &A,
                                                 const VectorType2 &b,
                                                 const VectorType &x,
                                                 const VectorType2 &y) {
      VectorType2 tmp = b;
      subtractProduct(tmp, A, x);
      return tmp * y;
    }
  };

  template <class MatrixType>
  struct OperatorHelper<MatrixType, EnableMatrix<MatrixType>> {
    template <class VectorType, class VectorType2>
    static typename VectorType::field_type Axy(const MatrixType &A,
                                               const VectorType &x,
                                               const VectorType2 &y) {
      assert(x.size() == A.M());
      assert(y.size() == A.N());

      using Result = std::decay_t<decltype(y*y)>;

      Result result = 0;
      sparseRangeFor(A, [&](auto&& Ai, auto i) {
        // ToDo: Provide a specialization using a single temporary
        // for the case that each y[i] has the same type.
        auto Aix = y[i];
        Aix = 0;
        sparseRangeFor(Ai, [&](auto&& Aij, auto j) {
          addProduct(Aix, Aij, x[j]);
        });
        result += Aix * y[i];
      });
      return result;
    }

    template <class VectorType, class VectorType2>
    static typename VectorType::field_type bmAxy(const MatrixType &A,
                                                 const VectorType2 &b,
                                                 const VectorType &x,
                                                 const VectorType2 &y) {
      assert(x.size() == A.M());
      assert(y.size() == A.N());
      assert(y.size() == b.size());

      using Result = std::decay_t<decltype(y*y)>;

      Result result = 0;
      sparseRangeFor(A, [&](auto&& Ai, auto i) {
        // ToDo: Provide a specialization using a single temporary
        // for the case that each b[i] has the same type.
        auto Aix = b[i];
        sparseRangeFor(Ai, [&](auto&& Aij, auto j) {
          subtractProduct(Aix, Aij, x[j]);
        });
        result += Aix * y[i];
      });
      return result;
    }
  };

  //! Compute \f$(Ax,y)\f$
  template <class OperatorType, class VectorType, class VectorType2>
  typename VectorType::field_type Axy(const OperatorType &A,
                                      const VectorType &x,
                                      const VectorType2 &y) {
    return OperatorHelper<OperatorType>::Axy(A, x, y);
  }

  //! Compute \f$(b-Ax,y)\f$
  template <class OperatorType, class VectorType, class VectorType2>
  typename VectorType::field_type bmAxy(const OperatorType &A,
                                        const VectorType2 &b,
                                        const VectorType &x,
                                        const VectorType2 &y) {
    return OperatorHelper<OperatorType>::bmAxy(A, b, x, y);
  }

} // end namespace MatrixVector
} // end namespace Dune

#endif // DUNE_MATRIX_VECTOR_AXY_HH
